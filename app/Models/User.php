<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @property int $id
 * @property string $username
 * @property string $phone
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'phone',
    ];

    public static function create(string $username, string $phone): static
    {
        $model = new static();

        $model->username = $username;
        $model->phone    = $phone;

        return $model;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function link()
    {
        return $this->hasOne(UserLink::class, 'user_id');
    }
}
