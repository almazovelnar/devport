<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $user_id
 * @property int $number
 * @property float $amount
 */
class History extends Model
{
    protected $table = 'history';

    public static function create(int $userId, int $number, float $amount)
    {
        $model = new static();

        $model->user_id = $userId;
        $model->number  = $number;
        $model->setAmount($amount);

        return $model;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount / 100;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount * 100;
    }

}
