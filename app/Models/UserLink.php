<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $days
 * @property string $link
 * @property boolean $status
 * @property \DateTime $expired_at
 */
class UserLink extends Model
{
    const PERIOD_DAYS = 7;
    protected $table = 'user_links';

    protected $fillable = ['status', 'link', 'expired_at'];

    public static function create(int $userId, string $link, ?int $period = null): static
    {
        $model = new static();

        $model->user_id = $userId;
        $model->link    = $link;
        $model->days    = $period ?? self::PERIOD_DAYS;
        $model->status  = true;

        return $model;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    public function isActive(): bool
    {
        return ($this->status && !$this->isExpired());
    }

    public function isExpired(): bool
    {
        return $this->expired_at < now();
    }

    public function resetExpiredAt(): void
    {
        $this->expired_at = now()->addDays(self::PERIOD_DAYS);
    }
}
