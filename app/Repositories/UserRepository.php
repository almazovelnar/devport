<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function getByUsername(string $username)
    {
        return User::query()->where(function ($query) use ($username) {
            $query->where('username', $username)
                ->orWhere('phone', $username);
        })->first();
    }
}
