<?php

namespace App\Repositories;

use App\Models\History;

class HistoryRepository
{
    public function getUserHistory(int $userId)
    {
        return History::query()
            ->where('user_id', $userId)
            ->limit(3)
            ->orderByDesc('id')
            ->get();
    }
}
