<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckLinkActive
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        if (!$user->link->isActive()) {
            return redirect()->route('page', $user->link->getLink())->with('notification', 'Your page is expired or disabled');
        }
        
        return $next($request);
    }
}
