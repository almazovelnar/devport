<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\LoginRequest;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct(
        private UserRepository $userRepository
    )
    {
    }

    public function form()
    {
        return view('auth.login');
    }

    public function index()
    {
        return view('home');
    }

    public function login(LoginRequest $request)
    {
        /** @var User $user */
        if ($user = $this->userRepository->getByUsername($request->username)) {
            $this->guard()->login($user);
        } else {
            return redirect()->back()->with('notification', 'User not found');
        }

        return redirect()->home();
    }
}
