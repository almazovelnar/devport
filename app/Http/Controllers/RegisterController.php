<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use App\Services\UserLinkService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\RegisterRequest;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;

    public function __construct(
        private UserService $userService,
        private UserLinkService $userLinkService
    )
    {
    }

    public function form()
    {
        return view('auth.register');
    }

    public function register(RegisterRequest $request)
    {
        DB::beginTransaction();
        try {
            $user = $this->userService->create($request);
            $this->userLinkService->create($user);
            $this->guard()->login($user);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::debug($e);
            return redirect()->back();
        }

        return redirect()->home();
    }
}
