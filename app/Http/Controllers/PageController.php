<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Repositories\HistoryRepository;
use App\Services\HistoryService;
use App\Services\LuckyService;
use App\Services\UserLinkService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class PageController extends Controller
{
    public function __construct(
        private HistoryRepository $historyRepository,
        private UserLinkService $userLinkService,
        private LuckyService $luckyService,
        private HistoryService $historyService
    )
    {
    }

    public function index()
    {
        return view('page', [
            'user' => Auth::user()
        ]);
    }

    public function history()
    {
        $attempts = $this->historyRepository->getUserHistory(Auth::id());
        return view('history', [
            'user'     => Auth::user(),
            'attempts' => $attempts
        ]);
    }

    public function lucky()
    {
        /** @var User $user */
        $user = Auth::user();

        try {
            $number = $this->luckyService->generateNumber();
            $amount = $this->luckyService->calculateAmount($number);
            $result = $this->luckyService->getResult($number);

            if ($result) {
                $this->historyService->create($user, $number, $amount);
            }


            $message = View::make('partials.lucky-result', compact('number', 'amount', 'result'))
                ->render();

            return redirect()->route('page', $user->link->getLink())
                ->with('message', $message);
        } catch (\Exception $e) {
            Log::debug($e);
            return redirect()->route('page', $user->link->getLink());
        }
    }

    public function regenerate()
    {
        /** @var User $user */
        $user = Auth::user();

        try {
            $link = $this->userLinkService->regenerate($user);
        } catch (\Exception $e) {
            Log::debug($e);
            return redirect()->route('page', $user->link->getLink());
        }

        return redirect()->route('page', $link->getLink())->with('notification', 'Your page is regenerated successfully!');
    }

    public function disable()
    {
        /** @var User $user */
        $user = Auth::user();
        $link = $user->link->getLink();

        try {
            $this->userLinkService->disable($user);
        } catch (\Exception $e) {
            Log::debug($e);
            return redirect()->route('page', $link);
        }

        return redirect()->route('page', $link)->with('notification', 'Your page is disabled');
    }

    public function enable()
    {
        /** @var User $user */
        $user = Auth::user();
        $link = $user->link->getLink();

        try {
            $this->userLinkService->enable($user);
        } catch (\Exception $e) {
            Log::debug($e);
            return redirect()->route('page', $link);
        }

        return redirect()->route('page', $link)->with('notification', 'Your page is activated');
    }
}
