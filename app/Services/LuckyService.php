<?php
declare(strict_types=1);

namespace App\Services;

class LuckyService
{
    public const RESULT_PERCENTS = [
        900 => 70,
        600 => 50,
        300 => 30,
        0 => 10,
    ];

    public function generateNumber(): int
    {
        return rand(0, 1000);
    }

    public function calculateAmount(int $number): float
    {
        foreach (self::RESULT_PERCENTS as $result => $percent) {
            if ($number > $result) {
                return $number * $percent / 100;
            }
        }
        return 0;
    }

    public function getResult(int $number): bool
    {
        return ($number % 2) == 0;
    }
}
