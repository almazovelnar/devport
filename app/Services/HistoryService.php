<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\History;
use App\Models\User;

class HistoryService
{
    public function create(User $user, int $number, float $amount)
    {
        $model = History::create($user->id, $number, $amount);
        $model->save();
    }
}
