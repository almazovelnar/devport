<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;


class UserService
{
    public function create(Request $request)
    {
        $model = User::create(
            $request->username,
            $request->phone,
        );

        $model->save();

        return $model;
    }
}
