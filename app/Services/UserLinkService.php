<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\User;
use App\Models\UserLink;

class UserLinkService
{
    public function create(User $user): UserLink
    {
        $link = UserLink::create($user->id, $this->generateLink());
        $link->resetExpiredAt();
        $link->save();

        return $link;
    }

    protected function generateLink(): string
    {
        return md5(uniqid());
    }

    public function regenerate(User $user)
    {
        /** @var UserLink $link */
        $link = $user->link;
        $link->update([
            'link' => $this->generateLink(),
            'expired_at' => now()->addDays($link->days)
        ]);

        return $link;
    }

    public function disable(User $user)
    {
        $link = $user->link;
        $link->update(['status' => false]);

        return $link;
    }

    public function enable(User $user)
    {
        $link = $user->link;
        $link->update([
           'status' => true,
           'expired_at' => now()->addDays($link->days)
        ]);

        return $link;
    }
}
