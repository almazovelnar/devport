<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SiteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SiteController::class, 'index'])->name('home');

Route::get('login', [LoginController::class, 'form'])->name('login');
Route::post('login', [LoginController::class, 'login'])->name('login.submit');
Route::get('register', [RegisterController::class, 'form'])->name('register');
Route::post('register', [RegisterController::class, 'register'])->name('register.submit');
Route::get('logout', [LoginController::class, 'logout'])->name('logout');

Route::prefix('page')->group(function () {

    Route::middleware('check-link-active')->group(function () {
        Route::get('lucky', [\App\Http\Controllers\PageController::class, 'lucky'])
            ->name('lucky');

        Route::get('history', [\App\Http\Controllers\PageController::class, 'history'])
            ->name('history');
    });


    Route::get('regenerate', [\App\Http\Controllers\PageController::class, 'regenerate'])
        ->name('page.regenerate');

    Route::get('disable', [\App\Http\Controllers\PageController::class, 'disable'])
        ->name('page.disable');

    Route::get('enable', [\App\Http\Controllers\PageController::class, 'enable'])
        ->name('page.enable');

    Route::get('{slug}', [\App\Http\Controllers\PageController::class, 'index'])
        ->where('slug', '[\w\-]+')
        ->middleware('check-link-token')
        ->name('page');
});
