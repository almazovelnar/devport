let _alert = $(".alert:not(.visible)");
if (_alert.length) {
    _alert.animate({opacity: 1.0}, 2000).fadeOut("slow");
}
