@extends('layout')

@section('content')
    <div class="container">
        @include('partials.notification')
        <div class="d-flex justify-content-between align-items-center">
            <h2><b>Hello</b> "{{ $user->getUsername() }}"</h2>

            <div class="d-flex">
                <div>
                    <a href="{{ route('page.regenerate') }}" class="btn btn-primary btn-sm">Regenerate page</a>
                </div>

                <div style="margin-left: 10px;">
                    @if($user->link->isActive())
                        <a href="{{ route('page.disable') }}" class="btn btn-danger btn-sm text-white">Disable page</a>
                    @else
                        <a href="{{ route('page.enable') }}" class="btn btn-success btn-sm text-white">Activate page</a>
                    @endif
                </div>

            </div>
        </div>

        <br>

        <div class="d-flex">
            <div>
                <a href="{{ route('home') }}" class="btn btn-success btn-sm">Home</a>
            </div>


            @if($user->link->isActive())
                <div style="margin-left: 10px;">
                    <a href="{{ route('lucky') }}" class="btn btn-warning btn-sm">Imfeelinglucky</a>
                </div>

                <div style="margin-left: 10px;">
                    <a href="{{ route('history') }}" class="btn btn-secondary btn-sm text-white">History</a>
                </div>
            @endif
        </div>

        @if(session()->has('message'))
            <br>
            <div class="alert alert-secondary visible">{!! session('message') !!}</div>
        @endif
    </div>
@endsection
