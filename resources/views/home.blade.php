@extends('layout')

@section('content')
    <div class="container">
        @include('partials.notification')
        @if(!$user)
            <h1>Welcome to DevPort</h1>
            <p>Please login or create new account.</p>

            <a href="{{ route('login') }}" class="btn btn-success text-white">Login</a>
            <a href="{{ route('register') }}" class="btn btn-primary text-white">Sign up</a>
        @else
            <h2><b>Hello</b> "{{ auth()->user()->getUsername() }}"</h2>

            <div class="d-flex">
                <div>
                    <a href="{{ route('page', $user->link->getLink()) }}" class="btn btn-primary btn-sm text-white">My page</a>
                </div>

                <div style="margin-left: 10px;">
                    <a href="{{ route('logout') }}" class="btn btn-danger btn-sm text-white">Logout</a>
                </div>
            </div>
        @endif
    </div>
@endsection
