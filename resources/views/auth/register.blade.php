@extends('layout')


@section('content')
    <div class="container">
        @include('partials.notification')
        <h1 class="mb-3">Sign up</h1>
        <div class="mb-4">Already a member? <a href="{{ route('login') }}">Sign in</a></div>
        <form action="{{ route('register.submit') }}" method="POST">
            @csrf
            @method('POST')
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" name="username" value="{{ old('username') }}">

                        @error('username')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="phone">Phone number</label>
                        <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" value="{{ old('phone') }}">

                        @error('phone')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-success mt-3">Register</button>
                </div>
            </div>
        </form>
    </div>
@endsection
