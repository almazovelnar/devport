@extends('layout')


@section('content')
    <div class="container">
        @include('partials.notification')
        <h1 class="mb-3">Login</h1>
        <div class="mb-4">Sign in to DevProd or <a href="{{ route('register') }}">create an account</a></div>
        <form action="{{ route('login.submit') }}" method="POST">
            @csrf
            @method('POST')
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Username or Phone Number</label>
                        <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" name="username">
                        @error('username')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-success mt-3">Login</button>
                </div>
            </div>
        </form>
    </div>
@endsection
