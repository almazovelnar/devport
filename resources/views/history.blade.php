@extends('layout')

@section('content')
    <div class="container">
        <h1>History</h1>
        <p><a class="btn btn-info btn-sm text-white" href="{{ route('page', $user->link->getLink()) }}">Back</a></p>

        @include('partials.notification')
        <div>
            @if($attempts->isNotEmpty())
                <table class="table table-striped">
                    <tr>
                        <th>Username</th>
                        <th>Number</th>
                        <th>Amount</th>
                        <th>Date</th>
                    </tr>
                    @foreach($attempts as $attempt)
                        <tr>
                            <td>{{ $attempt->user->username }}</td>
                            <td>{{ $attempt->number }}</td>
                            <td>{{ $attempt->getAmount() }}$</td>
                            <td>{{ $attempt->created_at->format('d M, Y / H:i') }}</td>
                        </tr>
                    @endforeach
                </table>
            @else
                <p>Your history is empty</p>
            @endif
        </div>
    </div>
@endsection
