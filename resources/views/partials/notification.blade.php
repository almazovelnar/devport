@if(session()->has('notification'))
    <div class="alert alert-info">{{ session('notification') }}</div>
@endif
